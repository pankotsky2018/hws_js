let arr = ["Kharkiv", "Kiev", ["Borispol", [1, 2, 3], "Irpin"], "Odessa", "Lviv",["test", 1, 2], "Dnieper"];

let createList = (arr, parent = document.body) => {
    let ulist = document.createElement('ul');
    ulist.classList.add('list');
    parent.insertAdjacentElement('beforeend', ulist);

    arr.forEach(element => {
        if (Array.isArray(element)) {
            createList(element, ulist);            
        } else {
            let listItem = document.createElement('li');
            listItem.classList.add('list-item');
            listItem.innerHTML = `${element}`;
            ulist.insertAdjacentElement('beforeend', listItem);
        }
    });
}

createList(arr);
