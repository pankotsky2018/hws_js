const imgToShow = document.querySelector('.image-to-show');
const p = document.querySelector('.sec');
const IMAGES = ['./img/1.jpg', './img/2.jpg', './img/3.jpg', './img/4.jpg'];
let counter = 1;
let imgCounter = 1;
let sec = 3;

function ShowImage () {
    if (counter % 300 == 0) {
        imgToShow.setAttribute('src', `${IMAGES[imgCounter]}`);
        imgCounter ++;
        if (imgCounter == 4) {
            imgCounter = 0;
        }
    }
    counter ++;
    if (counter == 1201) {
        counter = 1;
    }
    // console.log(sec);
    p.textContent = `${Math.round(parseFloat(sec) * 100) / 100}`;
    sec -= 0.01;
    if (sec < 0) {
        sec = 3;
    }
};

setInterval(ShowImage, 10);
