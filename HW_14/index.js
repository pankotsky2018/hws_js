const changerTheme = document.querySelector('.svg');
const darkText = document.querySelectorAll('.dark');

function changeTheme () {
    changerTheme.classList.toggle('active');
    if (changerTheme.classList.contains('active')) {
        document.body.style.backgroundColor = 'grey';
        darkText.forEach(text => {
            text.style.color = 'white';
        });
    } else {
        document.body.style.backgroundColor = '';
        darkText.forEach(text => {
            text.style.color = '';
        });
    }
}

changerTheme.addEventListener('click', e => {
    changeTheme();
})