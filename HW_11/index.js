const eyes = document.querySelectorAll('.icon-password');
const form = document.querySelector('.password-form');
const textError = document.querySelector('.text');
    
form.addEventListener('submit', e => {
    e.preventDefault();
    const data = new FormData(form);
    if (!/^\S*$/.test(data.get('password')) || !/^\S*$/.test(data.get('re-password')) || data.get('password').length == 0 || data.get('re-password').length == 0) {
        textError.textContent = 'Введіть коретні дані!';
    } else if (data.get('password') !== data.get('re-password')) {
        textError.textContent = 'Потрібно ввести однакові значення!';
    } else {
        textError.innerText = '';
        setTimeout(() => alert('You are welcome!'));
    }
});

eyes.forEach(eye => {
    eye.addEventListener('click', e => {
        eyeClasses = e.target.classList;
        if (eyeClasses.contains('fa-eye')) {
            eyeClasses.replace('fa-eye', 'fa-eye-slash');
            e.target.previousElementSibling.setAttribute('type', 'text');
        } else {
            eyeClasses.replace('fa-eye-slash', 'fa-eye');
            e.target.previousElementSibling.setAttribute('type', 'password');
        }
    })
})