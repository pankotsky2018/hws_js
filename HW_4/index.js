let firstNum;
let secondNum;
let operator;
let result;

function calcUserInfo () {
    const operations = {
        '+': +firstNum + +secondNum,
        '-': +firstNum - +secondNum,
        '*': +firstNum * +secondNum,
        '/': +firstNum / +secondNum,
    }
    result = operations[operator];
    console.log(result);
}

function getUserInfo () {
    firstNum = prompt('Enter first number', firstNum);
    secondNum = prompt('Enter second number', secondNum);
    operator = prompt('Enter operation', operator);
    if (!!isNaN(firstNum) || !!isNaN(secondNum) || (operator !== '+' && operator !== '-' && operator !== '*' && operator !== '/')) {
        alert('Enter correct value!!!')
        getUserInfo();
    } else {
        calcUserInfo();
    }
}

getUserInfo();
