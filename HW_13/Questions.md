## Опишіть своїми словами різницю між функціями setTimeout() і setInterval()

setTimeout() виконує передану строку коду або колбек функцію один раз через вказаний проміжок часу
setInterval() виконує переданий строку коду або колбек функцію безліч разів з вказаним інтервалом часу, доки не зіткнеться з clearInteval

## Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?

Якщо передати нульову затримку, передана колбек ф-ція спрацює одразу після виконання поточного коду

## Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?

Щоб не завантажувати пам'ять комп'ютера.