## Опишіть своїми словами що таке Document Object Model (DOM)

Це програмний інтерфейс для HTML, XML та SVG файлів

## Яка різниця між властивостями HTML-елементів innerHTML та innerText?

El.innerHTML отримує або встановлює HTML розмітку всередені El.
El.innerText отримує або встановлює текстовий вміст всередені El.

## Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

* document.getElementBy*();
* element.querySelector();
* element.querySelectorAll();

Найуніверсальнійший спосіб element.querySelectorAll(); за рахунок гнучкого вибору селекторів та пошуку усіх елементів по вказаному селектору