// 1
let paragraph = document.querySelectorAll('p');

paragraph.forEach(item => item.style.backgroundColor = '#ff0000');
// 2

let optionsList = document.querySelector('#optionsList');
let optionsListParent = optionsList?.parentElement;

!!optionsList && console.log(optionsList);
!!optionsListParent && console.log('Parent:', optionsListParent);

if (!!optionsList) {
    for (let node of optionsList.childNodes) {
        let objNode = {'Child node': node, 'Type node': node.nodeName};
        console.log(objNode);
    }
}

// 3

let testParagraph = document.querySelector('#testParagraph');
testParagraph.classList.add('testParagraph');
testParagraph.textContent = 'This is a paragraph'

// 4

let mainHeaderChildren = document.querySelector('.main-header').children;

for (let elem of mainHeaderChildren) {
    elem.classList.add('nav-item');
    console.log('Children:', elem);
}

// 5

let sectionTitle = document.querySelectorAll('.section-title');

for (let elem of sectionTitle) {
    elem.classList.remove('section-title');
    console.log(elem);
}
