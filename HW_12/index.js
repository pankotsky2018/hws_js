const buttons = document.querySelectorAll('.btn');

window.addEventListener('keypress', e => {
    buttons.forEach(btn => {
        if (e.key.toLowerCase() === btn.textContent.toLocaleLowerCase()) {
            btn.focus();
        } else {
            btn.blur();
        }
    })
})

// window.addEventListener('keyup', e => {
//     buttons.forEach(btn => {
//         if (e.key.toLowerCase() === btn.textContent.toLocaleLowerCase()) {
//             btn.blur();
//         }
//     })
// })